//
//  AskNameController.swift
//  LunchApp
//
//  Created by Alina Taimasova on 10.06.2018.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import UIKit

class AskNameController: UIViewController {
    
    @IBOutlet weak var addNameField: UITextField!
    
    @IBAction func addNameBut(_ sender: Any) {
        performSegue(withIdentifier: "toMain", sender: AskNameController.self)
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
