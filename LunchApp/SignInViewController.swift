//
//  ViewController.swift
//  LunchApp
//
//  Created by Alina Taimasova on 09.06.2018.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import GoogleAPIClientForREST
import GoogleSignIn
import UIKit
import UserNotifications

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    var nameIs = String()
    var dateIs = String()
    var date = Timer()
    var myDate = Date()
    let output = UITextView()
    let signInButton = GIDSignInButton()
    var range = String()
    var userIndex = Int()
    var rangeFood = String()
    var dishesArray = [String]()
    
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheetsReadonly]
    private let service = GTLRSheetsService()
    
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var signView: GIDSignInButton!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var afterSignInHelloLabel: UILabel!
    @IBOutlet weak var signOutBut: UIButton!
    @IBOutlet weak var difNameLabel: UIButton!
    @IBOutlet weak var addName: UITextField!
    @IBOutlet weak var saveBut: UIButton!
    @IBOutlet weak var todayIsLab: UILabel!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var shareBut: UIButton!
    @IBOutlet weak var imageOne: UIImageView!
    @IBAction func saveName(_ sender: Any) {
        
        self.addName.isHidden = true
        self.saveBut.isHidden = true
        
        nameIs = addName.text!
        
        // Permanent Data Storage
        UserDefaults.standard.set(addName.text, forKey: "name")
        
        let nameObject = UserDefaults.standard.object(forKey: "name")
        
        if let name = nameObject as? String {
            
            nameIs = name
        }
        afterSignInHelloLabel.text = "Привет, " + nameIs + "!"
        
        menuDisplay()
    }
    
    
    @IBAction func setDifferentNameBut(_ sender: Any) {
        
        self.addName.isHidden = false
        self.saveBut.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Display saved name (if no logout was done)
        let nameObject = UserDefaults.standard.object(forKey: "name")
        
        if let name = nameObject as? String {
            
            nameIs = name
        }
        afterSignInHelloLabel.text = "Привет, " + nameIs + "!"
        
        // Display today's date
        date = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(dateDisplay), userInfo: nil, repeats: false)
        dateIs = dateAsString()
        
        // Configure Google Sign-in.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        signOutBut.isHidden = true
        self.afterSignInHelloLabel.isHidden = true
        self.difNameLabel.isHidden = true
        self.addName.isHidden = true
        self.saveBut.isHidden = true
        self.todayIsLab.isHidden = true
        self.menuLabel.isHidden = true
        self.shareBut.isHidden = true
    }
    
    // for Timer's selector in viewDidLoad
    @objc func dateDisplay() {
        
        let date = DateFormatter()
        date.dateFormat = "EEEE, dd MMMM"
        
        dateIs = date.string(from: Date())
    }
    
    
    // Transforming Date into String format for dispaying
    func dateAsString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMMM"
        let str = dateFormatter.string(from: Date() as Date)
        return str
    }
    
    // Transforming Date into String format for using in Sheet's Downloads
    func dateForSheet() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let strForDate = dateFormatter.string(from: Date())
        return strForDate
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            self.signInButton.isHidden = true
            self.signView.isHidden = true
            self.signOutBut.isHidden = false
            self.signOutBut.isEnabled = true
            self.welcomeLabel.isHidden = true
            self.output.isHidden = false
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            self.afterSignInHelloLabel.isHidden = false
            let fullName = user.profile.name
            self.afterSignInHelloLabel.isHidden = false
            self.difNameLabel.isHidden = false
            self.todayIsLab.isHidden = false
            self.menuLabel.isHidden = false
            self.imageOne.isHidden = true
            self.shareBut.isHidden = false
            self.imageTwo.isHidden = false
            
            if nameIs == "" {
                nameIs = fullName!
            }
            afterSignInHelloLabel.text = "Привет, " + nameIs + "!"
            
            menuDisplay()
            
            todayIsLab.text = "Сегодня " + dateIs + ".\nНа обед у тебя: "
            
        }
    }
    
    // spreadsheet:
    // https://docs.google.com/spreadsheets/d/1NrPDjp80_7venKB0OsIqZLrq47jbx9c-lrWILYJPS88/edit#gid=1441674361
    
    func menuDisplay() {
        output.text = "Загружаем..."
        menuLabel.text = output.text
        let spreadsheetId = "1NrPDjp80_7venKB0OsIqZLrq47jbx9c-lrWILYJPS88"
        
        if dateForSheet() == "вторник" {
            range = "Вторник!A1:M34"
        } else if dateForSheet() == "суббота" || dateForSheet() == "воскресенье" {
            range = "Пятница !A1:M34"
            menuLabel.text = "К счастью уже совсем скоро тебе на работу и сможешь налопаться от души!:)"
        } else {
            range = dateForSheet() + " !A1:M34" }
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
    
    }
    
    // Process the response and display output
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket,
                                       finishedWithObject result : GTLRSheets_ValueRange,
                                       error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
        let rows = result.values!
      
        var arrayCounter = Int()
        
        for row1 in rows {
           arrayCounter += 1
            var stringName = ""
            stringName += "\(row1[0])"
    
            if stringName == nameIs {
            

            var rowModifier = ""
                arrayCounter = arrayCounter - 1
                rowModifier += "\(rows[arrayCounter])"
                
                dishesArray = rowModifier.components(separatedBy: ", ")
                dishesArray.removeFirst()
                
                // finding right values on intersections
                var counter = Int()
                var menu = [String]()
                
                if dishesArray[0] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                }
                if dishesArray[1] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                if dishesArray[2] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                if dishesArray[3] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                }
                if dishesArray[4] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                if dishesArray[5] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                }
                if dishesArray[6] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                if dishesArray[7] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                if dishesArray[8] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                if dishesArray[9] == "1]" || dishesArray[9] == "1"  {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    
                }
                
                //in case if array have more elements
                 if row1.count > 9 {
                    dishesArray.append("")
                if dishesArray[10] == "1]" || dishesArray[10] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    }
                }
                if row1.count > 10 {
                    dishesArray.append("")
                if dishesArray[11] == "1]" || dishesArray[11] == "1" {
                    counter += 1
                    menu.append("\(rows[1][counter])")
                }
                else { counter += 1
                    }
                }
                var menuString = ""
                for cell in menu {
                    menuString += "\(cell) \n"
                }
                
                 menuLabel.text = "\(menuString)"
            break 
            } else {
               menuLabel.text = "А ты точно наш работник?"
            }
        }
        
        if rows.isEmpty {
            output.text = "No data found."
            return
        }
    }
    

    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
   
    @IBAction func signOut(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signOut()
        reNew()
        nameIs = ""
        UserDefaults.standard.removeObject(forKey: "name")
    }
    
     // Reload application data (renew root view )
    func reNew(){
       
        UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "Root")
    }
    
    @IBAction func share(_ sender: Any) {
        
        view.isUserInteractionEnabled = true
        let textToShare = NSLocalizedString("sharingText", comment: "")
        let activityController = UIActivityViewController(activityItems: [menuLabel.text!, textToShare], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    // Reminder about lunch
    func notification() {
        let notification = UNMutableNotificationContent()
        notification.title = "Ура, скоро обед!"
        notification.body = "Заходи глянуть, что в меню сегодня. "
        notification.sound = UNNotificationSound.default()
        
        var dateComponents = DateComponents()
        dateComponents.hour = 11
        dateComponents.minute = 45
       
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let notificationReq = UNNotificationRequest(identifier: "identifier", content: notification, trigger: trigger)
        
        UNUserNotificationCenter.current().add(notificationReq, withCompletionHandler: nil)
    }
    
    
    // Customizing Keyboard
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
    self.view.endEditing(true)
   }

   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    
    return true
  }
    
}
